package com.example.christian.project3;

import org.json.JSONObject;

public class MyCard {
    public String title;
    public String description;
    public String url;

    MyCard() {}
    MyCard(String t, String d, String u) {
        title = t;
        description = d;
        url = u;
    }

     static MyCard fromJSON(JSONObject j) {
         return new MyCard();
     }
}
