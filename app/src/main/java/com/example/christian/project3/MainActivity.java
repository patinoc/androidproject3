package com.example.christian.project3;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "YOUR_TWITTER_KEY";
    private static final String TWITTER_SECRET =
            "YOUR_SECRET_KEY";
    private static final String NEWS_API_KEY = "YOUR_NEWS_KEY";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<MyCard> cardList;
    private TwitterSession session;
    private String news_source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv);
        session = Twitter.getInstance().core.getSessionManager().getActiveSession();

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        cardList = new ArrayList<MyCard>();
        news_source =
                getSharedPreferences(LoginOptions.PREFS_NAME, Context.MODE_PRIVATE)
                        .getString("news_source", "ars-technica");
        if (news_source.equals("none")) {
            news_source = "ars-technica";
        }
        new JSONAsyncTest().execute("https://newsapi.org/v1/articles?source=" + news_source + "&apiKey=" +
                NEWS_API_KEY);

        //Check if user is logged in to Twitter/Facebook
        if (session != null)
            new TwitterAsync().execute();
        if (AccessToken.getCurrentAccessToken() != null)
            new FacebookAsync().execute();
        mAdapter = new MyCardAdapter(cardList);
        mRecyclerView.setAdapter(mAdapter);
    }

    public class FacebookAsync extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Void doInBackground(Void... params) {
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/feed",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            JSONObject jsonObject = response.getJSONObject();
                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); ++i) {
                                    cardList.add(FBCard.fromJSON(jsonArray.getJSONObject(i)));
                                }
                            }
                            catch (JSONException j) {
                                j.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
            return null;
        }

    }

    public class TwitterAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
            StatusesService statusesService = twitterApiClient.getStatusesService();
            Call<List<Tweet>> call =
                    statusesService.homeTimeline(5, null, null, false, true, false, true);
            call.enqueue(new Callback<List<Tweet>>() {
                @Override
                public void success(Result<List<Tweet>> result) {
                    for (Tweet t : result.data) {
                        TweetCard tweetCard = new TweetCard();
                        tweetCard.title = "@" + t.user.screenName;
                        tweetCard.description = t.text;
                        tweetCard.url = "http://www.twitter.com/" + t.user.screenName + "/status/" +
                                t.idStr;
                        cardList.add(tweetCard);
                    }
                    mAdapter = new MyCardAdapter(cardList);
                    mRecyclerView.setAdapter(mAdapter);
                }

                @Override
                public void failure(TwitterException exception) {
                    exception.printStackTrace();
                }
            });
            return null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, LoginOptions.class));
        }

        return super.onOptionsItemSelected(item);
    }
    public class JSONAsyncTest extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject res_json;
            try {
                res_json = new JSONObject(result);
                JSONArray articles = res_json.getJSONArray("articles");
                for (int i = 0; i < articles.length(); ++i) {
                    JSONObject j = articles.getJSONObject(i);
                    cardList.add(NewsCard.fromJSON(j));
                }
                mAdapter = new MyCardAdapter(cardList);
                mRecyclerView.setAdapter(mAdapter);
            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
