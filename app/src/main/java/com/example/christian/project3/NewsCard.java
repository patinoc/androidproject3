package com.example.christian.project3;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

public class NewsCard extends MyCard {
    public static String TITLE_TAG = "title";
    public static String DESCR_TAG = "description";
    public static String URL_TAG = "url";


    public NewsCard() {}
    public NewsCard(String t, String d, String nURL)
    {
        super(t,d,nURL);
    }

    @Nullable
    public static NewsCard fromJSON(JSONObject jsonObject)
    {
        try {
            String t = jsonObject.getString(TITLE_TAG);
            String d = jsonObject.getString(DESCR_TAG);
            String su = jsonObject.getString(URL_TAG);
            return new NewsCard(t,d,su);
        }
        catch(JSONException e) {
        }
        return null;
    }

}
