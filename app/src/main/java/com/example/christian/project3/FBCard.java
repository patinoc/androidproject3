package com.example.christian.project3;


import org.json.JSONException;
import org.json.JSONObject;

public class FBCard extends MyCard {
    public static String MSG_TAG = "message";
    public static String ID_TAG = "id";
    public static FBCard fromJSON(JSONObject c) {
        FBCard fbCard = new FBCard();
        fbCard.title = "Facebook Post";
        try {
            String[] tags = c.getString(ID_TAG).split("_");
            fbCard.description = c.getString(MSG_TAG);
            fbCard.url = "http://www.facebook.com/" + tags[0] + "/posts/" + tags[1] ;
        }
        catch(JSONException e) {
            e.printStackTrace();
        }
        return fbCard;

    }

}
